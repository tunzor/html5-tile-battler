# HTML5 Tile Battler #

This is a learning project to understand game development using HTML5 canvas and JavaScript.
Playable at http://tunzor.com/games/tile

## Learning points ##
* Very simple AI to move enemy and seek out the player
* Tile based game world and movement
* Pause functionality and pausing the game loop